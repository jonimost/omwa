## OMWa
This is a personal project for a office management platform. Allows creation of schedules, standups and follow up on productivity of teams.

### Summary
This Project involves two main parts;
- Django REST API (backend)
- ReactJS (frontend)

### Agile
The [Trello](https://trello.com/b/efD6eQW8/omwa) dashboard handles all tasks to be done. 

### Contributions
Communicate with me [email](john.nyingi@live.com) to be added as a contributor

### Author
John Nyingi