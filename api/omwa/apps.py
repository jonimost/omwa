from django.apps import AppConfig


class OmwaConfig(AppConfig):
    name = 'omwa'
